package budget.purchase;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Purchase {
    private final String name;
    private final BigDecimal price;

    public Purchase(String name, String price) {
        this.name = name;
        this.price = new BigDecimal(price);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("%s $%s", name, price.setScale(2, RoundingMode.HALF_UP));
    }
}
