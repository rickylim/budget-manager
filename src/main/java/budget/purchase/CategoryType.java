package budget.purchase;

import java.util.Arrays;

public enum CategoryType {
    FOOD("Food"),
    ENTERTAINMENT("Entertainment"),
    CLOTH("Clothes"),
    OTHER("Other"),
    ALL("All");

    private final String name;

    CategoryType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static CategoryType findByName(String name) {
        return Arrays.stream(CategoryType.values())
                .filter(categoryType -> categoryType.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(name + "not found"));
    }

}
