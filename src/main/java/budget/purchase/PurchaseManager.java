package budget.purchase;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PurchaseManager {
    private final String CATEGORY = "category";
    private final String NAME = "name";
    private final String PRICE = "price";
    private final String PURCHASE_FIELD_SEPARATOR = ";";
    private final List<String> PURCHASE_HEADERS = List.of(CATEGORY, NAME, PRICE);


    private PurchaseRecords purchaseRecords;

    public PurchaseManager() {
        purchaseRecords = new PurchaseRecords();
    }

    public PurchaseRecords getPurchaseRecords() {
        return purchaseRecords;
    }

    public void setPurchaseRecords(PurchaseRecords newRecords) {
        this.purchaseRecords = newRecords;
    }

    public void printAddPurchaseMenu() {
        System.out.println("Choose the type of purchase");
        System.out.println("1) Food");
        System.out.println("2) Clothes");
        System.out.println("3) Entertainment");
        System.out.println("4) Other");
        System.out.println("5) Back");
    }

    public void addPurchase() {
        printAddPurchaseMenu();
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) {
            System.out.println();
            int choice = scanner.nextInt();
            CategoryType categoryType;
            switch (choice) {
                case 1:
                    categoryType = CategoryType.FOOD;
                    break;
                case 2:
                    categoryType = CategoryType.CLOTH;
                    break;
                case 3:
                    categoryType = CategoryType.ENTERTAINMENT;
                    break;
                case 4:
                    categoryType = CategoryType.OTHER;
                    break;
                case 5:
                    return;
                default:
                    throw new RuntimeException(String.format("Category choice: %s, not found", choice));
            }
            Purchase purchase = readPurchase();
            purchaseRecords.addPurchase(categoryType, purchase);
            printAddPurchaseMenu();
        }
    }

    public Purchase readPurchase() {
        System.out.println("Enter purchase name:");

        Scanner scanner = new Scanner(System.in);
        String item = scanner.nextLine().trim();

        System.out.println("Enter its price:");
        String price = scanner.nextLine().trim();

        System.out.println("Purchase was added!");
        System.out.println();

        return new Purchase(item, price);
    }

    public void printPurchaseMenu() {
        System.out.println("Choose the type of purchase");
        System.out.println("1) Food");
        System.out.println("2) Clothes");
        System.out.println("3) Entertainment");
        System.out.println("4) Other");
        System.out.println("5) All");
        System.out.println("6) Back");
    }

    public void showPurchases() {
        printPurchaseMenu();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            System.out.println();

            int choice = scanner.nextInt();
            CategoryType categoryType;
            switch (choice) {
                case 1:
                    categoryType = CategoryType.FOOD;
                    break;
                case 2:
                    categoryType = CategoryType.CLOTH;
                    break;
                case 3:
                    categoryType = CategoryType.ENTERTAINMENT;
                    break;
                case 4:
                    categoryType = CategoryType.OTHER;
                    break;
                case 5:
                    categoryType = CategoryType.ALL;
                    break;
                case 6:
                    return;
                default:
                    throw new RuntimeException(String.format("Category choice: %s, not found", choice));
            }

            if (Objects.equals(categoryType, CategoryType.ALL)) {
                var purchases = purchaseRecords.getCategoricalPurchases().stream()
                        .map(CategoricalPurchase::getPurchases)
                        .filter(purchase-> !purchase.isEmpty())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
                var allCategoricalPurchases = new CategoricalPurchase(categoryType.getName(), purchases);
                allCategoricalPurchases.showPurchases();
            } else {
                var categoricalPurchases = purchaseRecords.getCategoricalPurchase(categoryType);
                categoricalPurchases.showPurchases();
            }

            printPurchaseMenu();
        }

    }

    public BigDecimal countTotal() {
        return purchaseRecords.getCategoricalPurchases().stream()
                .map(CategoricalPurchase::countTotal)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<String> generatePurchaseTextEntries() {
        String headerText = generateHeaderText();
        List<String> purchaseTexts = generatePurchaseTexts();

        List<String> textEntries = new ArrayList<>();
        textEntries.add(headerText);
        textEntries.addAll(purchaseTexts);

        return textEntries;
    }

    private String generateHeaderText() {
        return String.join(PURCHASE_FIELD_SEPARATOR, PURCHASE_HEADERS);
    }

    private List<String> generatePurchaseTexts() {
        return purchaseRecords.getCategoricalPurchases().stream()
                .filter(categoricalPurchase -> !categoricalPurchase.getPurchases().isEmpty())
                .map(categoricalPurchase -> categoricalPurchase.createPurchaseTexts(PURCHASE_FIELD_SEPARATOR))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public PurchaseRecords extractPurchaseRecord(BufferedReader reader) throws IOException {
        PurchaseRecords result = new PurchaseRecords();

        List<String> headers = Arrays.asList(reader.readLine().split(PURCHASE_FIELD_SEPARATOR));
        if (!headers.equals(PURCHASE_HEADERS)) {
            throw new RuntimeException(String.format("Expected header: %s. But got: %s", PURCHASE_HEADERS, headers));
        }

        String line;
        while ((line = reader.readLine()) != null) {
            List<String> purchaseEntries = Arrays.asList(line.split(PURCHASE_FIELD_SEPARATOR, PURCHASE_HEADERS.size()));
            String category = purchaseEntries.get(PURCHASE_HEADERS.indexOf(CATEGORY));
            String name = purchaseEntries.get(PURCHASE_HEADERS.indexOf(NAME));
            String price = purchaseEntries.get(PURCHASE_HEADERS.indexOf(PRICE));

            CategoryType categoryType = CategoryType.findByName(category);
            result.addPurchase(categoryType, new Purchase(name, price));
        }

        return result;
    }

}