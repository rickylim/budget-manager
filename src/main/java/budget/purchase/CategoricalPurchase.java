package budget.purchase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CategoricalPurchase {
    private final String categoryName;
    private final List<Purchase> purchases;

    public CategoricalPurchase(String name) {
        this.categoryName = name;
        purchases = new ArrayList<>();
    }

    public CategoricalPurchase(String name, List<Purchase> purchases) {
        this.categoryName = name;
        this.purchases = purchases;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<Purchase> getPurchases() {
        return Collections.unmodifiableList(purchases);
    }

    public void addPurchase(Purchase p) {
        purchases.add(p);
    }

    public void showPurchases() {
        if (purchases.isEmpty()) {
            System.out.println("The purchase list is empty");
            System.out.println();
            return;
        }

        System.out.println(categoryName + ":");
        purchases.forEach(System.out::println);

        var total = countTotal();
        if (BigDecimal.ZERO.compareTo(total) != 0) {
            String totalTxt = String.format("Total sum: $%s", total.setScale(2, RoundingMode.HALF_UP));
            System.out.println(totalTxt);
            System.out.println();
        }
    }

    public BigDecimal countTotal() {
        return  purchases.stream()
                .map(Purchase::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);

    }

    public List<String> createPurchaseTexts(String purchaseFieldSeparator) {
        return purchases.stream()
                .map(purchase -> createPurchaseText(purchaseFieldSeparator, purchase))
                .collect(Collectors.toList());
    }

    private String createPurchaseText(String fieldSeparator, Purchase purchase) {
        return String.join(fieldSeparator, categoryName, purchase.getName(), purchase.getPrice().toString());
    }

}
