package budget.purchase;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class PurchaseRecords implements Iterable<CategoricalPurchase> {

    private final LinkedHashMap<CategoryType, CategoricalPurchase> records;

    public PurchaseRecords() {
        records = Arrays.stream(CategoryType.values())
                .collect(Collectors.toMap(
                        categoryType -> categoryType,
                        categoryType -> new CategoricalPurchase(categoryType.getName()),
                        (u, v) -> u, LinkedHashMap::new)
                );
    }

    public void addPurchase(CategoryType categoryType, Purchase purchase) {
        CategoricalPurchase cp = records.get(categoryType);
        cp.addPurchase(purchase);
    }

    public CategoricalPurchase getCategoricalPurchase(CategoryType categoryType) {
        if (!records.containsKey(categoryType)) {
            throw new IllegalArgumentException(String.format("Records'keyset has no %s as its key", categoryType));
        }

        return records.get(categoryType);
    }

    public List<CategoricalPurchase> getCategoricalPurchases() {
        return List.copyOf(records.values());
    }


    @Override
    public Iterator<CategoricalPurchase> iterator() {
        return records.values().iterator();
    }

}
