package budget;


import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        Path budgetFilePath = Paths.get("purchases.txt");
        BudgetManager budgetManager = new BudgetManager(budgetFilePath);
        budgetManager.readAction();
    }
}