package budget;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IncomeManager {
    private final List<BigDecimal> incomes = new ArrayList<>();

    public void addIncome(Integer amount) {
        incomes.add(BigDecimal.valueOf(amount).setScale(2, RoundingMode.HALF_UP));
    }

    public void readEntry() {
        System.out.println("Enter income:");
        Scanner scanner = new Scanner(System.in);
        Integer inputAmount = scanner.nextInt();
        addIncome(inputAmount);

        System.out.println("Income was added!");
        System.out.println();
    }

    public String generateTotalIncomeText() {
        String TOTAL_HEADER = "Total Income: ";
        return String.format("%s $%s", TOTAL_HEADER, countTotal().toString());
    }

    /*
    A possible input:
    Total Income: $1000.00
     */
    public Integer extractTotalIncome(String line) {
        String totalIncomeText = line.split(":")[1]
                .replace("$", "")
                .trim();
        return new BigDecimal(totalIncomeText).intValue();
    }

    public void setTotalIncome(Integer income) {
        incomes.clear();
        addIncome(income);
    }

    public BigDecimal countTotal() {
        return incomes.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
    }

}
