package budget;

import budget.analyze.AnalyzeManager;
import budget.analyze.SortingPurchaseAnalyzeMethod;
import budget.purchase.PurchaseManager;
import budget.purchase.PurchaseRecords;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class BudgetManager {
    private final IncomeManager incomeManager = new IncomeManager();
    private final PurchaseManager purchaseManager = new PurchaseManager();
    private final AnalyzeManager analyzeManager = new AnalyzeManager();
    private final Path budgetFilePath;

    public BudgetManager(Path budgetFilePath) {
        this.budgetFilePath = budgetFilePath;
    }

    public void printActionMenu() {
        System.out.println("Choose you action:");
        System.out.println("1) Add income");
        System.out.println("2) Add purchase");
        System.out.println("3) Show list of purchases");
        System.out.println("4) Balance");
        System.out.println("5) Save");
        System.out.println("6) Load");
        System.out.println("7) Analyze (Sort)");
        System.out.println("0) Exit");
    }

    public void readAction() {
        printActionMenu();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            System.out.println();
            int input = scanner.nextInt();
            switch (input) {
                case 1:
                    incomeManager.readEntry();
                    break;
                case 2:
                    purchaseManager.addPurchase();
                    break;
                case 3:
                    purchaseManager.showPurchases();
                    break;
                case 4:
                    printBalance();
                    break;
                case 5:
                    save();
                    break;
                case 6:
                    load();
                    break;
                case 7:
                    sortPurchases();
                    break;
                case 0:
                    System.out.println("Bye!");
                    System.exit(0);
            }
            readAction();
        }
    }

    public void printBalance() {
        var result = incomeManager.countTotal().subtract(purchaseManager.countTotal());

        if (result.compareTo(BigDecimal.ZERO) < 0) {
            result = BigDecimal.ZERO;
        } else {
            result = result.setScale(2, RoundingMode.HALF_UP);
        }

        String balanceTxt = String.format("Balance: $%s", result);
        System.out.println(balanceTxt);
        System.out.println();
    }

    public void save() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(budgetFilePath.toString(), false))) {
            writer.println(incomeManager.generateTotalIncomeText());

            for (String purchase : purchaseManager.generatePurchaseTextEntries()) {
                writer.println(purchase);
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("IO exception occurs when SAVING file: %s%n", budgetFilePath));
        }

        System.out.println("Purchases were saved!" + System.lineSeparator());
        System.out.println();
    }

    public void load() {
        try (BufferedReader reader = Files.newBufferedReader(budgetFilePath)) {
            Integer totalIncome = incomeManager.extractTotalIncome(reader.readLine());
            incomeManager.setTotalIncome(totalIncome);

            PurchaseRecords records = purchaseManager.extractPurchaseRecord(reader);
            purchaseManager.setPurchaseRecords(records);
        } catch (IOException e) {
            throw new RuntimeException(String.format("IO exception occurs when LOADING file: %s%n", budgetFilePath));
        }

        System.out.println("Purchases were loaded");
        System.out.println();

    }

    public void sortPurchases() {
        var purchaseRecords = purchaseManager.getPurchaseRecords();
        var analyzeMethod = new SortingPurchaseAnalyzeMethod(purchaseRecords);
        analyzeManager.setMethod(analyzeMethod);
        analyzeManager.analyze();
    }

}
