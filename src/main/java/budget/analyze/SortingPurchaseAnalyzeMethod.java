package budget.analyze;

import budget.purchase.CategoricalPurchase;
import budget.purchase.CategoryType;
import budget.purchase.Purchase;
import budget.purchase.PurchaseRecords;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SortingPurchaseAnalyzeMethod implements AnalyzeMethod {
    private final PurchaseRecords purchaseRecords;

    public SortingPurchaseAnalyzeMethod(PurchaseRecords purchaseRecords) {
        this.purchaseRecords = purchaseRecords;
    }

    private int readSortMethod() {
        System.out.println("How do you want to sorted?");
        System.out.println("1) Sort all purchases");
        System.out.println("2) Sort by type");
        System.out.println("3) Sort certain type");
        System.out.println("4) Back");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        return scanner.nextInt();
    }

    public void analyze() {
        int choice = readSortMethod();

        switch (choice) {
            case 1:
                sortAll();
                break;
            case 2:
                sortByType();
                break;
            case 3:
                sortAType();
                break;
            case 4:
                return;
            default:
                throw new IllegalArgumentException(String.format("Sort method: %s, not found", choice));
        }

        analyze();
    }


    private void sortAll() {
        var purchases = purchaseRecords.getCategoricalPurchases().stream()
                .map(CategoricalPurchase::getPurchases)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        sortPurchases(CategoryType.ALL.getName(), purchases);
    }

    private void sortAType() {
        var categoryType = readCategoryType();
        var categoricalPurchase = purchaseRecords.getCategoricalPurchase(categoryType);

        var purchases = categoricalPurchase.getPurchases();

        sortPurchases(categoryType.getName(), purchases);
    }

    private void sortPurchases(String category, List<Purchase> purchases) {
        var sortedPurchases = purchases.stream()
                .sorted(Comparator.comparing(Purchase::getPrice).reversed())
                .collect(Collectors.toList());

        var categoricalPurchase = new CategoricalPurchase(category, sortedPurchases);
        categoricalPurchase.showPurchases();
    }

    private CategoryType readCategoryType() {
        System.out.println("Choose the type of purchase");
        System.out.println("1) Food");
        System.out.println("2) Clothes");
        System.out.println("3) Entertainment");
        System.out.println("4) Other");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        System.out.println();
        switch (choice) {
            case 1:
                return CategoryType.FOOD;
            case 2:
                return CategoryType.CLOTH;
            case 3:
                return CategoryType.ENTERTAINMENT;
            case 4:
                return CategoryType.OTHER;
            default:
                throw new IllegalArgumentException(String.format("Input category: %s, not found", choice));
        }
    }

    private void sortByType() {
        var categoricalPurchases = purchaseRecords.getCategoricalPurchases();
        var sortedCategoricalPurchases = categoricalPurchases.stream()
                .filter(categoricalPurchase -> !Objects.equals(categoricalPurchase.getCategoryName(), CategoryType.ALL.getName()))
                .sorted(Comparator.comparing(CategoricalPurchase::countTotal).reversed())
                .collect(Collectors.toList());

        System.out.println("Types:");
        for (var categoricalPurcase : sortedCategoricalPurchases) {
            System.out.printf("%s - $%s%n", categoricalPurcase.getCategoryName(), categoricalPurcase.countTotal());
        }

        var total = sortedCategoricalPurchases.stream()
                .map(CategoricalPurchase::countTotal)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
        String totalTxt = String.format("Total sum: $%s", total);
        System.out.println(totalTxt);
        System.out.println();
    }

}
