package budget.analyze;


public class AnalyzeManager {
    private AnalyzeMethod method;

    public void setMethod(AnalyzeMethod method) {
        this.method = method;
    }

    public void analyze() {
        this.method.analyze();
    }
}
