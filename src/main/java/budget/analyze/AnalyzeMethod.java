package budget.analyze;

public interface AnalyzeMethod {
    void analyze();
}
