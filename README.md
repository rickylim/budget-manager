# Budget manager

Budget manager is a simple CLI to manage your budget.
This application manages your budget by tracking your income and expenses using a filesystem.
The created file is located at your working directory and named as `purchases.txt`.

# Learning outcomes

- Basic concepts of Java programming
- Java Filesystem API
- Collections API
- Sort API

# Build

```bash
./gradlew clean jar
```

The `jar` is available `build/libs/budget-manager-1.0.jar`

## Demo

A file `purchases.txt` will be created when you choose to save from the menu.

```bash
java -jar build/libs/budget-manager-1.0.jar
```